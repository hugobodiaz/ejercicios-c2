function par(x){ 
    if (x%2===0){ 
    return "par"; 
    }else{ 
    return "impar"; 
    } 
}
    

function mayor(a,b){

    if(a>b)
    {
        return a +" es mayor que " + b;
    }
    else
    {
        return b + " es mayor que " + a;
    }
}

function datos(x)
{
    var stringCompleto = x + " es ";

    if (x%2===0)
    {
        stringCompleto+= "par ";
    }
    else{
        stringCompleto+= "impar ";
    }

    if (x%3===0)
    {
        stringCompleto+= "SI es divisible por 3 ";
    }
    else{
        stringCompleto+= "NO es divisible por 3 ";
    }

    if (x%5===0)
    {
        stringCompleto+= "SI es divisible por 5 ";
    }
    else{
        stringCompleto+= "NO es divisible por 5 ";
    }

    if (x%7==0)
    {
        stringCompleto+= "SI es divisible por 7";
    }
    else{
        stringCompleto+= "NO es divisible por 7";
    }

    return stringCompleto; 
}

function sumaValores(param){
    var totalsum = 0;
    param.forEach(element => {
        totalsum+=element;
    });

    return totalsum;
}

function factorial(x){
    var totalfactorial=1;
    var i = 1;
    while(i<=x)
    {
        totalfactorial*=i;
        ++i;
    }
    return totalfactorial;
}

function primo(x){
    var esPrimo = "el " + x + " es un numero primo";
    var i = x-1;
    while(i>1)
    {
        if(x % i === 0)
        {
            esPrimo = "el " + x + " no es un numero primo";
        }
        i--;
    }

    return esPrimo;
}

function fibonacci(x){
    
    var returnString = "1";

    var num1 = 0;
    var num2 = 1;
    var resultado = 0;
    for(var i = 2; i <= x; i++)
    {
        resultado = num1+num2;
        num1=num2;
        num2=resultado;   
        returnString += ", " + num2;
    }
    return returnString;
}

function primoCifras(x){
    var startingnumber = Math.pow(10,x-1);
    
    var prime=false;
    while(prime==false)
    {
        var prime = true;
        var i = startingnumber-1;
        while(i>1)
        {
            if(startingnumber % i === 0)
            {
                prime = false;
            }
            i--;
        }
        startingnumber++;
    }

    return startingnumber-1;

}

function capitaliza(x){
 
    var mayuscula = x.substring(0,1).toUpperCase();
    var minuscula = x.substring(1).toLowerCase();

    return mayuscula+minuscula;

}

function palabra(x){
    var amountOfLetters = x.length;
    var amountOfVowels = 0;
    for(var i = 0; i<amountOfLetters;i++)
    {
        //otro metodo para contar instancias dentro de un string (mas complejo)
        //var amountOfVowels = (x.match(/a/g) || []).length;

        if( x[i] === 'a'||x[i] === 'e'||x[i] === 'i'||x[i] === 'o'||x[i] === 'u' ||
            x[i] === 'A'||x[i] === 'E'||x[i] === 'I'||x[i] === 'O'||x[i] === 'U')
        {
            amountOfVowels++;

        }
    }

    var amountOfConsonants = amountOfLetters-amountOfVowels;
    var parity = par(amountOfLetters);

    var stringReturn =  "'" + x + "' tiene " + amountOfLetters + " letras." + amountOfLetters + " es un numero " + parity + ". Vocales: " + amountOfVowels + ". Consonantes: " + amountOfConsonants;

    return stringReturn;
}

function hoy(){
    var hoy = new Date();
    var diaSemana = hoy.getDay();

    switch(diaSemana)
    {
        case 0: return "Hoy es Domingo";
        case 1: return "Hoy es Lunes";
        case 2: return "Hoy es Martes";
        case 3: return "Hoy es Miercoles";
        case 4: return "Hoy es Jueves";
        case 5: return "Hoy es Viernes";
        case 6: return "Hoy es Sabado";
    }
}

function navidad(){

    var hoy = new Date();

    var xmasday=25;
    var xmasmonth=12;
    var xmasyear = hoy.getFullYear();

    var navidad = new Date();

    navidad.setMonth(xmasmonth-1);
    navidad.setFullYear(xmasyear);
    navidad.setDate(xmasday);

    var diff = navidad.getTime()-hoy.getTime();

    var differenciaDeDias = diff/ (1000 * 60 * 60 * 24)

    return "faltan " + differenciaDeDias + " para navidad";

}

function analiza(params){
    var suma = 0;
    var menor = params[0];
    var mayor = params[0];
    for(var i = 0; i<params.length;i++)
    {
        console.log(params[i]);
        suma+=params[i];
        if(params[i]>mayor)
        {
            mayor = params[i];
        }
        
        if(params[i]<menor)
        {
            menor = params[i];
        }
    }

    return "la suma de los " + params.length + " numeros es igual a " + suma + ", el menor numero es " + menor + " y el mayor numero es " + mayor;
}