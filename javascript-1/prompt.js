function promptExercise(){
    var number = Math.floor(Math.random() * 100) + 1;

    //console.log(number);
    var hasBeenGuessed = false;
    var attempts = 0;
    while(hasBeenGuessed == false)
    {
        attempts++;
        var attempt = prompt("adivina un numero del 1 al 100", 1);

        if(attempt ==number){
            hasBeenGuessed = true;
            return "Has adivinado el numero!, has necesitado "  + attempts + " intento(s)" ;
        }
        if(attempt > number)
        {
            console.log("Demasiado alto, vuelve a intentar...");
        }
        if(attempt < number)
        {
            console.log("Demasiado bajo, vuelve a intentar...");
        }
    }
    
}